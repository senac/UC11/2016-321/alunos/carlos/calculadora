package com.example.casa.calculator;

import android.widget.TextView;

/**
 * Created by CASA on 25/08/2017.
 */



public class Display {

    private TextView textView;
    private boolean limpaDisplay = true ;
    java.text.NumberFormat nf = java.text.NumberFormat.getInstance() ;

    public Display(TextView display) {
        this.textView = display;
    }

    public TextView getTextView() {
        return textView;
    }



    public boolean isLimpaDisplay() {
        return limpaDisplay;
    }

    public void setLimpaDisplay(boolean limpaDisplay) {
        this.limpaDisplay = limpaDisplay;
    }

    public String getText(){
        return  this.textView.getText().toString() ;
    }

    public void setText(String texto){
        this.textView.setText(texto);
    }

    public void setText(double valor){
        this.setText(nf.format(valor));
    }


    public double getValue() {
        return Double.parseDouble(this.getText());
    }














}
