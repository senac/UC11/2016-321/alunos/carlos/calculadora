package com.example.casa.calculator;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Calculator extends AppCompatActivity {

    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btnPonto;
    private Button btnLimpar;
    private Button btnSomar;
    private Button btnSubtracao;
    private Button btnMultiplicacao;
    private Button btnDivisao;
    private Button bntIgual;
    private TextView textViewDisplay;
    Display display;


    private static final char ADICAO = '+';
    private static final char SUBTRACAO = '-';
    private static final char DIVISAO = '/';
    private static final char MULTIPLICACAO = '*';


    private double operando1 = Double.NaN;
    private double operando2;
    private char operacao;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.i("#CALCULADORA#", "Chamando onCreate ....");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);


        this.btn0 = (Button) findViewById(R.id.btn0);
        this.btn1 = (Button) findViewById(R.id.btn1);
        this.btn2 = (Button) findViewById(R.id.btn2);
        this.btn3 = (Button) findViewById(R.id.btn3);
        this.btn4 = (Button) findViewById(R.id.btn4);
        this.btn5 = (Button) findViewById(R.id.btn5);
        this.btn6 = (Button) findViewById(R.id.btn6);
        this.btn7 = (Button) findViewById(R.id.btn7);
        this.btn8 = (Button) findViewById(R.id.btn8);
        this.btn9 = (Button) findViewById(R.id.btn9);
        this.btnPonto = (Button) findViewById(R.id.btnPonto);

        this.textViewDisplay = (TextView) findViewById(R.id.testViewdisplay);

        display = new Display(this.textViewDisplay);

        ClickDigito clickDigito = new ClickDigito(display);

        this.btn0.setOnClickListener(clickDigito);
        this.btn1.setOnClickListener(clickDigito);
        this.btn2.setOnClickListener(clickDigito);
        this.btn3.setOnClickListener(clickDigito);
        this.btn4.setOnClickListener(clickDigito);
        this.btn5.setOnClickListener(clickDigito);
        this.btn6.setOnClickListener(clickDigito);
        this.btn7.setOnClickListener(clickDigito);
        this.btn8.setOnClickListener(clickDigito);
        this.btn9.setOnClickListener(clickDigito);

        this.btnPonto.setOnClickListener(clickDigito);

        // implementar botao limpar

        // atribuir bota na instancia da classe
        this.btnLimpar = (Button) findViewById(R.id.btnLimpar);

        //adicionar tratador do evento de click
        this.btnLimpar.setOnClickListener(
                // criação de um classe anonima
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        limparDisplay(true);

                    }
                }

        );


        // atribuir bota na instancia da classe
        this.btnSomar = (Button) findViewById(R.id.btnSomar);
        this.btnSubtracao = (Button) findViewById(R.id.btnSubtracao);
        this.btnDivisao = (Button) findViewById(R.id.btnDivisao);
        this.btnMultiplicacao = (Button) findViewById(R.id.btnMultiplicacao);
        this.bntIgual = (Button) findViewById(R.id.btnIgual);

        //adicionar tratador do evento de click
        this.btnSomar.setOnClickListener(new View.OnClickListener() {

            // criação de um classe anonima
            @Override
            public void onClick(View v) {                //pegar operacao
                operacao = ADICAO;
                calculado = false;
                calcular();

            }
        });

        this.btnSubtracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = SUBTRACAO;
                calculado = false;
                calcular();
            }
        });

        this.btnDivisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = DIVISAO;
                calculado = false;
                calcular();
            }
        });

        this.btnMultiplicacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacao = MULTIPLICACAO;
                calculado = false;
                calcular();
            }
        });

        this.bntIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                display.setLimpaDisplay(false);
                calculado = true;
                calcular();
            }
        });


    }


    private boolean calculado = false;


    private void calcular() {


        if (Double.isNaN(operando1)) {

            //pegar valor operando 1 e converter para double
            operando1 = display.getValue();


            limparDisplay(false);

        } else {


            if (!display.isLimpaDisplay()) {

                if (!calculado) {
                    operando2 = display.getValue();
                }



                switch (operacao) {
                    case ADICAO:
                        operando1 = operando1 + operando2;
                        break;
                    case SUBTRACAO:
                        operando1 = operando1 - operando2;
                        break;
                    case DIVISAO:

                        if (operando2 != 0) {

                            operando1 = operando1 / operando2;

                        } else {

                            Context context = getApplicationContext();
                            String mensagem = "Impossível dividir por zero!";
                            int duracao = Toast.LENGTH_LONG;

                            Toast toast = Toast.makeText(context, mensagem, duracao) ;
                            // toast.setGravity(Gravity.TOP, 0, 0); Definir posicao do toast
                            toast.show();

                        }


                        break;
                    case MULTIPLICACAO:
                        operando1 = operando1 * operando2;
                        break;

                }


                limparDisplay(false);
                display.setText(operando1);

            }
        }

    }

    private void limparDisplay(boolean zerar) {

        if (zerar) {
            display.setText("0");
            operando1 = Double.NaN;
            calculado = false;

        }

        display.setLimpaDisplay(true);


    }


}
