package com.example.casa.calculator;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.Button;

/**
 * Created by CASA on 25/08/2017.
 */

public class ClickDigito implements View.OnClickListener{


    private Display display ;


    public ClickDigito(Display display){
        this.display = display ;
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {
        String texto  = display.getText();
        Button botao = (Button)v ;
        String digito = botao.getText().toString() ;

        if(display.isLimpaDisplay()) {
            display.setText(digito); //4
            display.setLimpaDisplay(false);

        }else{
            if( botao.getId() != R.id.btnPonto ) {
                display.setText(texto + digito); //42.
            }else{
                if(!texto.contains(".")){
                    display.setText(texto + digito);
                }
            }
        }
    }
}
